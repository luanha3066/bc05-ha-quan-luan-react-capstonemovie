import axios from "axios";
import { createConfig } from "./configURL";
import { BASE_URL } from "./configURL";
export const movieService = {
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getBanner: () => {
    return axios({
      url: `${BASE_URL}api/QuanLyPhim/LayDanhSachBanner`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
