import React from "react";
import { message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { userService } from "../../service/userService";
import BG from "../../assets/backapp.b46ef3a1.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  setFormData,
  setFormErrors,
  setIsValid,
} from "../../Redux-toolkit/slice/formRegSlice";

export default function SignUpPage() {
  const navigate = useNavigate();
  const dispact = useDispatch();
  const formData = useSelector((state) => state.formRegSlice.data);
  const formErrors = useSelector((state) => state.formRegSlice.errors);
  const isFormValid = useSelector((state) => state.formRegSlice.isFormValid);
  const handleChange = (event) => {
    const { name, value } = event.target;
    let errors = Object.assign({}, formErrors);
    dispact(setFormData({ [name]: value }));
    //   kiểm tra rỗng + mk < 6
    if (value.trim() === "") {
      errors = { ...errors, [`${name}Err`]: "Trường này không thể để trống" };
    } else if (name === "matKhau" && value.trim().length < 6) {
      errors = {
        ...errors,
        [`matKhauErr`]: "Mật khẩu phải có ít nhất 6 ký tự",
      };
    }
    //   xác nhận mật khẩu
    else if (name === "matKhauXacNhan" && formData.matKhau !== value) {
      errors = {
        ...errors,
        [`matKhauXacNhanErr`]: "Mật khẩu không trùng khớp",
      };
    }
    // ktra họ tên
    else if (name === "hoTen" && /\d/.test(value)) {
      errors = {
        ...errors,
        [`hoTenErr`]: "Họ tên không thể chứa chữ số",
      };
    } else {
      delete errors[`${name}Err`];
    }
    dispact(setFormErrors(errors));
    if (Object.keys(errors).length === 0) {
      dispact(setIsValid(true));
    } else {
      dispact(setIsValid(false));
    }
    console.log("errors: ", errors);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("isFormValid: ", isFormValid);
    if (!isFormValid) {
      return;
    } else {
      const { name, value } = event.target;
      let errors = Object.assign({}, formErrors);
      dispact(setFormData({ [name]: value }));
      userService
        .postDangKy(formData)
        .then((res) => {
          message.success("Đăng ký thành công, vui lòng đăng nhập lại");
          console.log(res);
          setTimeout(() => {
            navigate("/login");
          }, 1000);
        })
        .catch((err) => {
          if (err.response.data.content === "Email đã tồn tại!") {
            errors = { ...errors, [`emailErr`]: "Email đã tồn tại!" };
            dispact(setFormErrors(errors));
          }
          message.error("Lỗi");
          console.log(err);
        });
    }
  };
  return (
    <>
      <div className="relative ">
        <img src={BG} alt="" />
      </div>
      <div className="rounded w-1/3 h-full fixed top-20 left-1/3 bg-white">
        <div className=" flex justify-center pt-3">
          <div className="bg-red-500 rounded-full h-10 w-10  ">
            <FontAwesomeIcon
              className="text-xl pt-2 text-white"
              icon="fa-solid fa-lock"
            />
          </div>
        </div>
        <div>
          <h4 className="font-medium text-xl">Đăng ký</h4>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form-group row pl-4 pt-4">
            <div className="col-11">
              <input
                type="text"
                name="taiKhoan"
                className="form-control h-14"
                placeholder="Tài Khoản *"
                value={formData.taiKhoan}
                onChange={handleChange}
              />
              {formErrors.taiKhoanErr && (
                <div className="text-red-500 text-left">
                  {formErrors.taiKhoanErr}
                </div>
              )}
            </div>
          </div>
          <div className="form-group row pl-4 ">
            <div className="col-11">
              <input
                type="password"
                className="form-control h-14"
                name="matKhau"
                placeholder="Mật khẩu *"
                value={formData.matKhau}
                onChange={handleChange}
              />
              {formErrors.matKhauErr && (
                <div className="text-red-500 text-left">
                  {formErrors.matKhauErr}
                </div>
              )}
            </div>
          </div>
          <div className="form-group row pl-4 ">
            <div className="col-11">
              <input
                type="password"
                className="form-control h-14"
                name="matKhauXacNhan"
                placeholder="Nhập lại mật khẩu *"
                value={formData.matKhauXacNhan}
                onChange={handleChange}
              />
              {formErrors.matKhauXacNhanErr && (
                <div className="text-red-500 text-left">
                  {formErrors.matKhauXacNhanErr}
                </div>
              )}
            </div>
          </div>
          <div className="form-group row pl-4 ">
            <div className="col-11">
              <input
                type="text"
                name="hoTen"
                className="form-control h-14"
                placeholder="Họ và tên *"
                value={formData.hoTen}
                onChange={handleChange}
              />
              {formErrors.hoTenErr && (
                <div className="text-red-500 text-left">
                  {formErrors.hoTenErr}
                </div>
              )}
            </div>
          </div>
          <div className="form-group row pl-4 ">
            <div className="col-11">
              <input
                type="email"
                name="email"
                className="form-control h-14"
                placeholder="Email *"
                value={formData.email}
                onChange={handleChange}
              />
              {formErrors.emailErr && (
                <div className="text-red-500 text-left">
                  {formErrors.emailErr}
                </div>
              )}
            </div>
          </div>
          <button
            type="submit"
            className="btn bg-red-500 w-96 text-white mt-3   "
          >
            Đăng Ký
          </button>
          <NavLink to="/login">
            <h5 className="mt-3 mr-5 text-right underline">
              Bạn đã có tài khoản? Đăng nhập
            </h5>
          </NavLink>
        </form>
      </div>
    </>
  );
}
