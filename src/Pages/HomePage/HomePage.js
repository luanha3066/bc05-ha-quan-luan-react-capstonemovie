import React, { useEffect, useState } from "react";
import { movieService } from "../../service/movieService";
import Banner from "./Banner/Banner";
import MovieList from "./MovieList/MovieList";

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        console.log(res);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const [bannerArr, setBannerArr] = useState([]);
  useEffect(() => {
    movieService
      .getBanner()
      .then((res) => {
        console.log(res);
        setBannerArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <div>
        <Banner bannerArr={bannerArr} />
      </div>
      <div className="container mx-auto">
        <MovieList movieArr={movieArr} />
      </div>
    </div>
  );
}
