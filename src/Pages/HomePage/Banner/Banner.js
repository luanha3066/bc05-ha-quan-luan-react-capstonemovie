import React from "react";
import { Carousel } from "antd";
const contentStyle = {
  margin: 0,
  height: "580px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
export default function Banner({ bannerArr }) {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };
  const renderBanner = () => {
    for (let i = 0; i < bannerArr.length; i++) {
      return (
        <Carousel afterChange={onChange} dotPosition="bottom" autoplay="true">
          <div>
            <img
              style={contentStyle}
              className=" w-full object-fill"
              src={bannerArr[i++].hinhAnh}
              alt=""
            />
          </div>
          <div>
            <img
              style={contentStyle}
              className=" w-full object-fill"
              src={bannerArr[i++].hinhAnh}
              alt=""
            />
          </div>
          <div>
            <img
              style={contentStyle}
              className=" w-full object-fill"
              src={bannerArr[i++].hinhAnh}
              alt=""
            />
          </div>
        </Carousel>
      );
    }
  };

  return <div>{renderBanner()}</div>;
}
