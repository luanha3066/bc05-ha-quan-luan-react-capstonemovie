import React from "react";
import { Card } from "antd";
const { Meta } = Card;
export default function MovieList({ movieArr }) {
  const renderMovieList = () => {
    return movieArr.slice(0, 15).map((item) => {
      console.log("item: ", item);
      return (
        <Card
          hoverable
          style={{
            width: 240,
          }}
          cover={
            <img
              className="h-80 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <Meta title={item.tenPhim} />
        </Card>
      );
    });
  };
  return <div className="grid grid-cols-5 gap-5">{renderMovieList()}</div>;
}
