import { message } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { setUserInfor } from "../../Redux-toolkit/slice/userSlice";
import { userLocalService } from "../../service/localStorageService";
import { userService } from "../../service/userService";
import BG from "../../assets/backapp.b46ef3a1.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  setFormData,
  setFormErrors,
} from "../../Redux-toolkit/slice/formSlice";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const formData = useSelector((state) => state.formSlice.data);
  const formErrors = useSelector((state) => state.formSlice.errors);
  const handleChange = (event) => {
    const { name, value } = event.target;
    let errors = Object.assign({}, formErrors);
    dispatch(setFormData({ [name]: value }));
    //   kiểm tra rỗng + mk < 6
    if (value.trim() === "") {
      errors = { ...errors, [`${name}Err`]: "Trường này không thể để trống" };
    } else if (value.trim().length < 6) {
      errors = {
        ...errors,
        [`matKhauErr`]: "Mật khẩu phải có ít nhất 6 ký tự",
      };
    } else {
      delete errors[`${name}Err`];
    }
    dispatch(setFormErrors(errors));
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    userService
      .postDangNhap(formData)
      .then((res) => {
        dispatch(setUserInfor(res.data.content));
        console.log(res);
        message.success("Đăng Nhập Thành Công");
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Lỗi");
        console.log(err);
      });
    console.log("formData: ", formData);
  };
  return (
    <>
      <div className="relative ">
        <img src={BG} alt="" />
      </div>
      <div className="rounded w-1/3 h-4/6 fixed top-1/4 left-1/3 bg-white">
        <div className=" flex justify-center pt-3">
          <div className="bg-red-500 rounded-full h-10 w-10  ">
            <FontAwesomeIcon
              className="text-xl pt-2 text-white"
              icon="fa-solid fa-user"
            />
          </div>
        </div>
        <div>
          <h4 className="font-medium text-xl">Đăng nhập</h4>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form-group row pl-4 pt-4">
            <div className="col-11">
              <input
                type="text"
                name="taiKhoan"
                className="form-control h-14"
                id="staticEmail"
                placeholder="Tài Khoản *"
                onChange={handleChange}
                value={formData.taiKhoan}
              />
              {formErrors.taiKhoanErr && (
                <div className="text-red-500 text-left">
                  {formErrors.taiKhoanErr}
                </div>
              )}
            </div>
          </div>
          <div className="form-group row pl-4 ">
            <div className="col-11">
              <input
                type="password"
                className="form-control h-14"
                name="matKhau"
                id="inputPassword"
                placeholder="Mật khẩu *"
                value={formData.matKhau}
                onChange={handleChange}
              />
              {formErrors.matKhauErr && (
                <div className="text-red-500 text-left">
                  {formErrors.matKhauErr}
                </div>
              )}
            </div>
          </div>
          <div className="form-check pl-5 flex justify-start">
            <label className="form-check-label">
              <input
                type="checkbox"
                className="form-check-input"
                name
                id
                defaultValue="checkedValue"
                defaultChecked
              />
              Nhớ tài khoản
            </label>
          </div>
          <button
            type="submit"
            className="btn bg-red-500 w-96 text-white mt-5 "
          >
            Đăng Nhập
          </button>
          <NavLink to="/signup">
            <h5 className="mt-3 mr-5 text-right underline">
              Bạn chưa có tài khoản? Đăng ký
            </h5>
          </NavLink>
        </form>
      </div>
    </>
  );
}
