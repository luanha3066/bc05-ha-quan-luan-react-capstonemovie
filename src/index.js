import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { rootReducer } from "./Redux/reducer/rootReducer";
import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./Redux-toolkit/slice/userSlice";
import formSlice from "./Redux-toolkit/slice/formSlice";
import formRegSlice from "./Redux-toolkit/slice/formRegSlice";
export const store_toolkit = configureStore({
  reducer: {
    userSlice: userSlice,
    formSlice: formSlice,
    formRegSlice: formRegSlice,
  },
});
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store_toolkit}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
