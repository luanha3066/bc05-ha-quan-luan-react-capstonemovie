import { userLocalService } from "../../service/localStorageService";
import { SET_USER_INFOR } from "../constant/constant";
const initailState = {
  userInfor: userLocalService.get,
};
export const userReducer = (state = initailState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFOR:
      return { ...state, userInfor: payload };
    default:
      return state;
  }
};
