import React from "react";
import { NavLink } from "react-router-dom";
import logo from "../../assets/logo.png";
import UserNav from "./UserNav";
export default function Header() {
  return (
    <div className="flex justify-between items-center px-10">
      <NavLink to="/">
        <img src={logo} className="h-16 w-52" />
      </NavLink>

      <div className="flex justify-between items-center px-10 ">
        <NavLink>
          <span className="px-3 hover:text-red-500 duration-500 text-sm font-medium ">
            Lịch Chiếu
          </span>
        </NavLink>
        <NavLink>
          <span className="px-3 hover:text-red-500 duration-500 text-sm font-medium ">
            Cụm Rạp
          </span>
        </NavLink>
        <NavLink>
          <span className="px-3 hover:text-red-500 duration-500 text-sm font-medium ">
            Tin Tức
          </span>
        </NavLink>
        <NavLink>
          <span className="px-3 hover:text-red-500  duration-500 text-sm font-medium ">
            Ứng Dụng
          </span>
        </NavLink>
      </div>

      <UserNav />
    </div>
  );
}
