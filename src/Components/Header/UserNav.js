import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalService } from "../../service/localStorageService";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userSlice.userInfor;
  });
  let handleLogOut = () => {
    userLocalService.remove();
    window.location.href = "/login";
  };
  const renderContent = () => {
    if (user) {
      return (
        <>
          <span className="px-1 border-black border-5  hover:text-red-500 duration-500 font-bold ">
            {user.hoTen}
          </span>
          <button
            onClick={handleLogOut}
            className="px-5 border-black border-5  hover:text-red-500 duration-500 font-bold "
          >
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <div className="flex">
            <NavLink to="/login">
              <div className="flex pr-2 border-r-2 hover:no-underline">
                <div className="bg-gray-400   rounded-full h-8 w-8  ">
                  <FontAwesomeIcon
                    className="text-xl pt-2 text-white"
                    icon="fa-solid fa-user"
                  />
                </div>
                <button className="pl-2  text-gray-400  hover:text-red-500  font-medium ">
                  Đăng Nhập
                </button>
              </div>
            </NavLink>
            <NavLink to="/signup">
              <div className="flex pl-3">
                <div className="bg-gray-400  rounded-full h-8 w-8   ">
                  <FontAwesomeIcon
                    className="text-xl pt-2 text-white"
                    icon="fa-solid fa-user"
                  />
                </div>
                <button className="pl-2 border-none  text-gray-400  hover:text-red-500  font-medium  ">
                  Đăng Ký
                </button>
              </div>
            </NavLink>
          </div>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
